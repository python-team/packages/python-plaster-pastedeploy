Source: python-plaster-pastedeploy
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Nicolas Dandrimont <olasd@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-pastedeploy,
               python3-all,
               python3-pytest,
               python3-setuptools,
               python3-plaster
Standards-Version: 4.7.0
Homepage: https://github.com/pylons/plaster_pastedeploy
Vcs-Git: https://salsa.debian.org/python-team/packages/python-plaster-pastedeploy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-plaster-pastedeploy
Testsuite: autopkgtest-pkg-python

Package: python3-plaster-pastedeploy
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Enhances: python3-plaster
Description: PasteDeploy configuration format plugin for plaster - Python 3.x module
 plaster is a loader interface around arbitrary config file formats. It exists
 to define a common API for applications to use when they wish to load
 configuration settings.
 .
 plaster_pastedeploy is a plaster plugin that provides a plaster.Loader that can
 parse ini files according to the standard set by PasteDeploy. It supports the
 wsgi plaster protocol, implementing the plaster.protocols.IWSGIProtocol
 interface.
 .
 This package provides the Python 3 version of the module.
